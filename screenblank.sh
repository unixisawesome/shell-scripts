#!/bin/sh
export DISPLAY=:0
export XAUTHORITY=/home/glitch/.Xauthority

result=`sysctl -n hw.sensors.aps0.indicator2|cut -b 1-3`

case $result in
  "Off" )
	xset dpms force off;;
  "On " )
	xset dpms force on;;
esac

#EOF
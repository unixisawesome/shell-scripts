while true; do
    read -p "SHUTDOWN?  " yn
    case $yn in
        [Yy]* ) mpv --really-quiet /media/lubuntu/ARCH_201906/hasta.mp3; wait 1; sudo halt -p;;
        [Nn]* ) exit 1;;
        * ) echo "Please answer yes or no.";;
    esac
done
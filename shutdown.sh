plug=$((`sysctl -n hw.sensors.acpibat0.raw0|cut -b 1`))
var=$((`sysctl -n hw.sensors.acpibat0.watthour3|cut -f 1 -d '.' `*100/45))

if [ $var -le 6 ]
   then 
	if [ $plug == 2 ]
		then
			exit 1	
		else
			/sbin/halt -p
	fi
   else 
       exit 1
fi